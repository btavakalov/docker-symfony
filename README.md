# Docker Symfony (PHP7-FPM - NGINX - Postges - Bower)

## Installation

0. Clone this repo

    ```bash
    git clone git@bitbucket.org:btavakalov/docker-symfony.git my_symfony_project
    ```

1. Copy `.env` 

    ```bash
    cp .env.dist .env
    ```

2. Create new Symfony app

    ```bash
    ./up.sh
    ./create.sh
    ```
    
    or clone your existed app to symfony dir
     
    ```bash
    git clone git://github.com/your/symfony_app.git symfony
    ./up.sh
    ```
    
    
## Useful commands

### php container bash

```bash
docker-compose exec php bash
```


### Composer (e.g. composer update)

```bash
docker-compose exec php composer update
```


### SF commands
```bash
docker-compose exec php sf cache:clear
```
