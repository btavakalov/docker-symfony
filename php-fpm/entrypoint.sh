#!/usr/bin/env bash

chmod -R 777 var/cache var/logs var/sessions web

composer install

bower install --allow-root

#php bin/console doctrine:schema:update --force
php bin/console doctrine:migrations:migrate --no-interaction
php bin/console doctrine:fixtures:load -n --append
php bin/console assets:install --symlink
php bin/console cache:clear --env=prod
php bin/console cache:clear --env=dev

chmod -R 777 var/cache var/logs var/sessions web

php-fpm
